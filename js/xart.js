jQuery.noConflict();


/***
* Sledování odkazů přes GA - lze použít jak na vnitřni, tak venkovni odkazy
* xart:ml
* @param category
* @param event
* @param label
* @param url
*
* @returns {Boolean}
*/
function gatrackeventnormalwaitforurl(category, event, label, url) {
	// mame ga?
	if(window._gat){
		// nastav callback
		_gaq.push(['_set','hitCallback',
							function(){document.location=url;}
		]);
		// registruj udalost
		_gaq.push(['_trackEvent', category, event, label]);
		// zrus pokracovani odkazu - bude provedeno asynchronnim callbackem
		return false;
	}
	// nemame - normalne klikni
	else {
		return true;
	}
}

function DateDiff(date1, date2) {
	console.log('DateDiff('+date1+", "+date2+")");
	var datediff = date1.getTime() - date2.getTime(); //store the getTime diff - or +
	console.log('DateDiff = '+datediff+', ie. '+(datediff / (24*60*60*1000))+' days');
	return parseInt(datediff / (24*60*60*1000)); //Convert values to -/+ days and return value
}

function calculateDays()
{
	if (jQuery('form#xreklamy input#date_from').length == 0 || jQuery('form#xreklamy input#date_from').length == 0) {
		return false;
	}

	var date_from = jQuery('form#xreklamy input#date_from').val();
	var date_to = jQuery('form#xreklamy input#date_to').val();
	var price = jQuery('form#xreklamy span.price').html();

	if (date_from != '' && date_to != '') {
		date_from = date_from.split(' ');
		var date1 = new Date(jQuery.trim(parseInt(date_from[2])), jQuery.trim(parseInt(date_from[1])-1), jQuery.trim(parseInt(date_from[0])));

		date_to = date_to.split(' ');
		var date2 = new Date(jQuery.trim(parseInt(date_to[2])), jQuery.trim(parseInt(date_to[1])-1), jQuery.trim(parseInt(date_to[0])));



		var days = DateDiff(date2, date1);

		if (days >= 0 && parseInt(price) > 0) {
			jQuery('form#xreklamy input#days').attr('value', days + 1);
			jQuery('form#xreklamy input#price').attr('value', (days + 1) * price + ' Kč');
		} else {
			jQuery('form#xreklamy input#days').attr('value', 0);
			jQuery('form#xreklamy input#price').attr('value', '0 Kč');
		}
	}
}

// xart:ps - formulář finálové kolo - výhry
function processCheckboxesFinalRound()
{
	var count_checked = jQuery('form.finale-vyhry fieldset.vyhry input[type="checkbox"]:checked').length;
	if (count_checked >= 2) {
		jQuery('form.finale-vyhry fieldset.vyhry input[type="checkbox"]').not(':checked').attr('disabled', 'disabled');
	} else {
		jQuery('form.finale-vyhry fieldset.vyhry input[disabled="disabled"]').attr('disabled', false);
	}
}


// xart:ml 2014-11-10 rozšíření třídy string o metodu Contains
if ( !String.prototype.contains ) {
	String.prototype.contains = function() {
		return String.prototype.indexOf.apply( this, arguments ) !== -1;
	};
}

jQuery(document).ready(function (){

	// xart:ml | 2016-08-26 | Na hlasování o dřevastavbách skrýt ten horní pannel, pokud obsahuje stejnou výzvu, jaká je prezentovaná i uvnitř hlasování
	var for_arch = jQuery(".mod_banners-top .id235 object").attr('data');
	if (for_arch !== undefined) {
		if (for_arch.indexOf("ARCH_1000x100_hlasovani") !== -1) { // banner exisutje, ma se skryt?
			if (window.location.href.indexOf(".cz/drevostavba-roku") !== -1) { // jsme na adrese s hlasovanim?
				// yes, skryjeme a nastavíme css tak, aby bylo vše OK
				jQuery(".mod_banners-top").hide();
				jQuery(".wrapper").css('margin-top', '70px'); // defaultní hodnoty z .css
				jQuery(".header0").css('top', '-65px'); // defaultní hodnoty z .css
			}	

		}
	}
	
	// xart:ml 2016-01-19
	jQuery(".mod_custom-detail-soutez .vote a.track").click(function() {
	  	var link = this.href;
	  	var name = jQuery(this).data("name");
	   	gatrackeventnormalwaitforurl('Drevostavba-roku', 'hlasovani', name, link);
	});

	if (document.location.href.contains('aktualni-cislo')) {
		jQuery(".switch-view").hide();
		jQuery("div.objednat-casopis").css("min-height", "0px");
		//xart:hk | 2015-09-30 | tento styl je už v css, navíc pokud se nastaví tady v css už s ním nejde nic dělat.
		//jQuery("div.objednat-casopis a").css("position", "absolute");
		jQuery("div.objednat-casopis a").css("right", "0px");
	}
	// xart:ml 2014-11-10 automatické zrušení limitu stránkování při vyhledávání
	jQuery("form.search_result .display #limit").val('0');

	// xart:ml 2014-11-10 Tištěné časopisy: "zvýraznění aktivních položek"
	var tistene_casopisy_aktivni = false;
	jQuery(".header1 .mod_mainmenu ul li.item-361 li.level-1 h3 a").each( function( key, value ) {
		if (document.location.href.contains(jQuery(this).attr('href'))) {
			tistene_casopisy_aktivni = true;
		}
	});
	if (tistene_casopisy_aktivni) {
		jQuery('.header1 .mod_mainmenu ul li.item-361').addClass('act');
	}

	// xart:ps - aktuální číslo - přepínač

	var aktualni_cisla = Array('ds', 'sr');
	var random_index = aktualni_cisla[Math.floor(Math.random()*aktualni_cisla.length)];
	jQuery('.mod_custom-casopis ul.items li.' + random_index).css({'display': 'none', 'position': 'absolute', 'left': '210px'});

	jQuery('.mod_custom-casopis ul.tabs li').click(function(){
		//jQuery('.mod_custom-casopis ul.tabs li').removeClass('act');
		//jQuery(this).addClass('act');
		var el_class = jQuery(this).attr('class').split(' ')[0];
		jQuery('.mod_custom-casopis ul.items > li').not('.' + el_class).css({'z-index': '0'});
		jQuery('.mod_custom-casopis ul.items li.' + el_class).show().animate({"left": '0'}).css({'position': 'relative','z-index': '99999'});
		jQuery('.mod_custom-casopis ul.items > li').not('.' + el_class).hide().css({'left': '210px', 'position' : 'absolute'});


	});


	// xart:ps - centrování elementu - využito pro popup
	if (jQuery("#popup_content").length > 0) {
		jQuery("#popup_content").css("position","absolute");
		jQuery("#popup_content").css("top", Math.max(0, ((jQuery(window).height() - jQuery("#popup_content").outerHeight()) / 2) +
		jQuery(window).scrollTop()) + "px");
		jQuery("#popup_content").css("left", Math.max(0, ((jQuery(window).width() - jQuery("#popup_content").outerWidth()) / 2) +
		jQuery(window).scrollLeft()) + "px");
	}

	// xart:ps - formulář finálové kolo - výhry
	if (jQuery('form.finale-vyhry').length > 0) {

		// antispam
		jQuery('form.finale-vyhry .question-wrapper input').attr('value', '12');
		jQuery('form.finale-vyhry .question-wrapper').hide();
		jQuery('form.finale-vyhry fieldset label.text').css('width', 'auto');


		// kontrola počtu vybraných výher
		processCheckboxesFinalRound();
		jQuery('form.finale-vyhry fieldset.vyhry input[type="checkbox"]').change(function(){
			processCheckboxesFinalRound();
		});

	}

	// xart:ps - efekt přidán do katalogu firem a obrázkového výpisu projektů
	jQuery('.xkatalog .listing.listing-image .item.adresar').hover(function (){
		jQuery('.overlay',this).stop(true,true).animate({'left':'6px'},{easing:'InOutExpo'},200);
	},function (){
		jQuery('.overlay',this).animate({'left':'-220px'},200);
	});
	// xart:ps - efekt přidán do nejoblíbenější dřevostavby z katalogu
	jQuery('.xkatalog .listing.listing-image .item.projekty').hover(function (){
		jQuery('.overlay',this).stop(true,true).animate({'left':'2px'},{easing:'InOutExpo'},200);
	},function (){
		jQuery('.overlay',this).animate({'left':'-220px'},200);
	});

	// xart:ps - bannery na titulce prolnout
	//jQuery('ul.bannery_prolnout').jcarousel({'scroll':1,'easing':'back','animation':600, 'size' : 2, 'auto' : 2});
	// xart:jk - deaktivováno v nové šabloně
	//jQuery('ul.bannery_prolnout').cycle({fx: 'fade', timeout: 11000});

	//jQuery('.mod_custom-casopis .prolnout').cycle({fx: 'fade', timeout: 6000});

	// xart:ps - antispam - nezávazná poptávka a nahlásit chybu u dřevostavby
	jQuery('form.contact-owner div.question, form.report div.question').hide();
	jQuery('form.contact-owner .question input, form.report .question input').attr('value', '4');

	if (jQuery('label.aid').length > 0) {
		var ins_value = jQuery('label.aid span').text();
		jQuery('input#aid').replaceWith('<input type="hidden" name="aid" id="aid" value="'+ ins_value +'" />');
		jQuery('label.aid').remove();
	}

	// xart:ps - zapnout fancybox
	jQuery('.fancybox').fancybox();
	// xart:ta | 2016-10-27 | fancyboxy v přehledu firem 
	jQuery('.fancybox-firma').fancybox({
		maxWidth: 960,
		helpers	: {
			title: {
				type: 'inside'
			}
		},
		afterLoad: function(){
			this.title = '<div class="fancybox-firma-content">' + jQuery(this.element).parent().find('.overlay').html() + '</div>';
			console.log(jQuery(this.element).html());
		}
	});

	// xart:ps - ověřování username při registraci
	if (jQuery('#combo-register').length > 0) {
		jQuery('#combo-register input#reg_field_name').val('');
		var timer;
		var timeout = 1000;
		jQuery('#combo-register input#reg_field_name').keyup(function(){
			clearTimeout(timer);
			var value = jQuery(this).val();
			if (jQuery(this).attr('value') !== '') {
				timer = setTimeout(function(){
					jQuery.ajax({
						type:'POST',
						url: "index.php?option=com_user&task=verify&format=raw",
						cache: false,
						data:{
							type: 'username',
							value: value
						},
						success: function(result){
							if (result === undefined || result === null) {
								alert('Nepovolené použití této funkce!');
							} else {
								var data = jQuery.parseJSON(result);
								var span_class = '';
								var message = '';
								if (data[0] === undefined) {
									span_class = 'ok';
									message = data[1];
								} else {
									span_class = 'error';
									message = data[0];
								}
								jQuery('#combo-register span#username_verify').css('display', 'inline-block').removeClass().addClass(span_class).html(message);
							}
						}
					});
				}, timeout);
			}
		})
	}

	// xart:ps - mapa v adresáři firem a projektech
	jQuery('.xkatalog .listing .googlecontainer, .xkatalog .manipulation .googlecontainer').hide();
	jQuery('.xkatalog .listing .toggle-map').click(function(){
		jQuery('.xkatalog .listing .googlecontainer').slideToggle('slow');
	});
	jQuery('.xkatalog .manipulation .toggle-map').click(function(){
		jQuery('.xkatalog .manipulation .googlecontainer').slideToggle('slow');
	});

	// diskusze kontrolní otázka
	// jQuery('#kcontrol-question-2 input').attr('value', 4);
	// jQuery('#kcontrol-question-2').hide();

	if (jQuery('#kcontrol-question-2').length > 0) {
		var ins_value = jQuery('#kcontrol-question-2 .kcol-first strong').text().split('(')[1];
		ins_value = ins_value.substr(0, ins_value.length - 1);

		jQuery('input[name="question"]').replaceWith('<input type="hidden" name="question" value="'+ ins_value +'" />');
		jQuery('#kcontrol-question-2').hide();
		jQuery('#kcontrol-question-2 .kcol-first').html('');
	}

	// kalendar v #xreklamy
	if (jQuery('#xreklamy').length > 0) {

		jQuery(function($){
			$.datepicker.regional['cs'] = {
				closeText: 'Zavřít',
				prevText: '&#x3C;Dříve',
				nextText: 'Později&#x3E;',
				currentText: 'Nyní',
				monthNames: ['leden','únor','březen','duben','květen','červen',
				'červenec','srpen','září','říjen','listopad','prosinec'],
				monthNamesShort: ['led','úno','bře','dub','kvě','čer',
				'čvc','srp','zář','říj','lis','pro'],
				dayNames: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
				dayNamesShort: ['ne', 'po', 'út', 'st', 'čt', 'pá', 'so'],
				dayNamesMin: ['ne','po','út','st','čt','pá','so'],
				weekHeader: 'Týd',
				dateFormat: 'dd.mm.yy',
				firstDay: 1,
				isRTL: false,
				showMonthAfterYear: false,
				yearSuffix: ''};
			$.datepicker.setDefaults($.datepicker.regional['cs']);

		});

		calculateDays();

		// xart:ps | reklamy | antispam
		jQuery('form#xreklamy tr.question').hide();
		jQuery('form#xreklamy input#question').attr('value', '2');

		// xart:sj | reklamy | live updates

		// xart:sj | reklamy | titulek
		jQuery('#name').change(function() {
			jQuery('.mod_custom-reklama.ukazka h3').html(jQuery('#name').val());
		});

		// xart:sj | reklamy | odkaz
		jQuery('#target_url').change(function() {
			jQuery('.mod_custom-reklama.ukazka a').attr('href', jQuery('#target_url').val());
		});

		// xart:sj | reklamy | text odkazu
		jQuery('#url').change(function() {
			jQuery('.mod_custom-reklama.ukazka a').html(jQuery('#url').val());
		});


		if (calendarCheck) {
			jQuery('#date_from').datepicker({
				defaultDate: '+6d',
				minDate: 6,
				dateFormat: 'd. m. yy',
				onClose: function (selectedDate) {
					jQuery('#date_to').datepicker('option', 'minDate', selectedDate);
				}
			});

			jQuery('#date_to').datepicker({
				defaultDate: '+7d',
				minDate: 6,
				dateFormat: 'd. m. yy',
				onClose: function(selectedDate) {
					jQuery('#date_from').datepicker('option', 'maxDate', selectedDate);
				}
			});
		} else {
			// xart:ps - pozor na čárky za posledním parametrem, toto není pole!
			// způsobovalo js chybu v IE7, a nešly pak otevřít obrázky ve fancyboxu
			jQuery('#date_from').datepicker({
				minDate: new Date(2013, 0, 1),
				dateFormat: 'd. m. yy'
			});

			jQuery('#date_to').datepicker({
				minDate: new Date(2013, 0, 1),
				dateFormat: 'd. m. yy'
			});
		}

		// xart:sj | reklamy | text a kontrola zadane delky
		jQuery('#text').change(function() {
			jQuery('.mod_custom-reklama.ukazka p').html(jQuery('#text').val());
		}).keypress(function(e) {
		var lengthF = jQuery(this).val();
			if (lengthF.length > 140){

				if(e.preventDefault){
					e.preventDefault()
				} else {
					e.stop()
				};
					e.returnValue = false;
					e.stopPropagation();
			}
		}).blur(function() {
			var lengthF = jQuery(this).val();
			if (lengthF.length > 140){
				jQuery(this).val(lengthF.substring(0, 140));
			}
		});

		// xart:sj | reklamy | nahrat testovaci obrazek
		jQuery('#image').change(function() {
			var thumb_url = jQuery('#thumb_uri').val();
			var form_url  = jQuery('#xreklamy').attr('action');
			jQuery('#xreklamy')
				.attr('target', 'thumb_frame').attr('action', thumb_url).submit()
				.attr('target', '_self').attr('action', form_url);
		});

		// xart:ps | reklamy | náhled
		// xart:sj | reklamy | nahled s uploadem
		jQuery('form#xreklamy button.nahled').click(function(){

			if (jQuery('.mod_custom-reklama.ukazka').css('display') != 'none') {
				jQuery('.mod_custom-reklama.ukazka').slideUp();
				return false;
			}

			var name = jQuery('form#xreklamy input#name').attr('value');
			var text = jQuery('form#xreklamy textarea#text').val();
			var url = jQuery('form#xreklamy input#url').attr('value');
			var target_url = jQuery('form#xreklamy input#target_url').attr('value');
			if (name !== '') {
				jQuery('.mod_custom-reklama.ukazka h3').html(name);
			}
			if (text !== '') {
				jQuery('.mod_custom-reklama.ukazka p').html(text);
			}
			if (url !== '') {
				jQuery('.mod_custom-reklama.ukazka a').html(url);
			}
			if (target_url !== '') {
				jQuery('.mod_custom-reklama.ukazka a').attr('href', target_url);
			}

			jQuery('.mod_custom-reklama.ukazka').slideDown();
			return false;
		});

		jQuery('#platce1').add('#platce0').change(function() {
			console.log('change '+jQuery(this).val());
			if (jQuery(this).val() == '1') {
				jQuery('.pouze-place').slideDown();
			} else {
				jQuery('.pouze-place').slideUp();
			}
		});
	} // if defined #xreklamy

	/* xart:ps | banners */
	// xart:ph - rozpohybování bannerů (2015-01-23)

	jQuery('.mod_custom-partners ul').prepend('<div class="prev"/><div class="next"/>');
	jQuery('.mod_custom-partners ul').each(function(){
		jQuery(this).cycle({
			slides : 'li',
			fx : 'carousel',
			timeout : 2200,
			speed: 400,
			easing : 'easeInOutQuad',
			prev : jQuery('.prev',this),
			next : jQuery('.next',this)
		});
	})

	// xart:ta | 2016-08-01 | slideshow fotografie a projekty v kartě firmy
	jQuery('.karta-firmy-fotogalerie.slideshow ul').prepend('<div class="prev"/><div class="next"/>');
	jQuery('.karta-firmy-fotogalerie.slideshow ul').each(function(){
		jQuery(this).cycle({
			slides : 'li',
			fx : 'carousel',
			timeout : 10000,
			speed: 400,
			easing : 'easeInOutQuad',
			prev : jQuery('.prev',this),
			next : jQuery('.next',this)
		});
	})
	jQuery('.karta-firmy-projekty-katalogu.slideshow ul').prepend('<div class="prev"/><div class="next"/>');
	jQuery('.karta-firmy-projekty-katalogu.slideshow ul').each(function(){
		jQuery(this).cycle({
			slides : 'li',
			fx : 'carousel',
			timeout : 12000,
			speed: 400,
			easing : 'easeInOutQuad',
			prev : jQuery('.prev',this),
			next : jQuery('.next',this)
		});
	})
	// xart:ta

	// xart:hk | 2016-11-28 | Podvlek s proklikem, přidáno html kvůli banneru nahoře
	// xart:ph | 2016-04-14 | Podvlek duben 2016 - má proklik
	// xart:hk | 2016-01-15 | Aktuální podvlek nemá proklik
	// xart:hk - proklik body
	// xart:fk | 2016-12-21 | presunuto na konec stranky v indexu šablony - pomoci php se predava url
	/*jQuery('body, html').click(function(e){
		if (e.target === this) {

			// do noveho okna
			window.open('http://www.drevoastavby.cz/casopis-drevo-a-stavby/aktualni-cislo');

			// do aktualniho okna
			//window.location = "http://www.drevoastavby.cz/online/e-das-2016-02-ochutnavka/";
		}
	});*/
	

	/* xart:ph | 2015-08-26 | Která obálka časopisu se Vám nejvíce líbí? - fake inputy - předávání akce do formuláře, kde jsou radiobuttony skryty */
	jQuery('span.fakeinput').css('display', 'block');				// nejdříve element zviditelníme - defaultně styly skrytý - kdyby neměli zapnutý js
	jQuery('.form_obalky form fieldset.jfradio').first().hide();

	jQuery('span.fakeinput input[name="fakeinput_obalky"]').click(function() {
		var val = jQuery(this).val();
		jQuery(this).parents('.article-content').find('.form_obalky form .jfradio input[value="'+val+'"]').click();
	});

	/* xart:hk | 2015-10-01 | Obsluha pro hodnocení pomocí líbí/nelíbí */
	jQuery('input[name="user_rating"]').click(function() {
		jQuery('#vote-form').submit();
	});

	/* xart:hk | 2016-01-22 | Hláška u hlasování ve výpisu článků */
    jQuery(".article_row, .leading").each( function () {
    	var row = this;
    	jQuery(this).find(".article_column #vote-form .content_vote .vote-dislike, .article_column #vote-form .content_vote .vote-like, #vote-form .content_vote .vote-like, #vote-form .content_vote .vote-dislike").click( function () {
    		jQuery(row).find("#vote-form").before('<div class="jv-vote-message" style="display: none;">Hlasovat můžete uvnitř článku.</div>');
    		jQuery(row).find("#vote-form").fadeOut(1);
    		jQuery(row).find(".article_column .jv-vote-message, .jv-vote-message").fadeIn(500);
    	})
    });

    /* xart:hk | 2016-02-24 | Padding článků ve výpisu */
    jQuery('.blog .article_row').each(function(){
    	if(jQuery(this).find('.commercial-article').length) {
    		jQuery(this).css('padding', '0');
    	}
    });

    /* xart:ph | 2016-07-19 | jquery tooltip - kvůli ověřené firmě 2016 */
    jQuery( ".tooltip" ).tooltip({
		// content: jQuery(this).attr('title')
		content: "<h2>Ověřený výrobce a dodavatel dřevostaveb</h2><p>Časopisy DŘEVO&stavby a sruby&roubenky tuto firmu <strong>doporučují</strong> na základě dlouhodobé spolupráce.</p><p>Redakce navštívila realizované domy a hovořila o <strong>zkušenostech</strong> s firmou <strong>s&nbsp;reálnými majiteli</strong> domů.</p><p><strong>Osobně</strong> jsme se  přesvědčili o <strong>kvalitách firmy</strong> a&nbsp;jejích referencích v&nbsp;uplynulých 24 měsících.</p><p class=\"vyzva\">Přečtěte si autentická vyjádření klientů této firmy.</p>"
	});


	if (jQuery(".info-o-firme #spolecnost_odkaz.reference").length) { // xart:ml doplnuji tridu reference, protoze se to hazelo i na samostatne reference.
		var chci_vypsat_referenci = false;

		if (window.location.hash) {
			// zjistim, jestli hash obsahuje referenci - odkaz na clanek. ten zacina ID - pomlcka - alias clanku
			// v podstate aby se to nezaplo pri normalnich hashich v textu
			var regex = /[0-9]+-/g;
			var str = window.location.hash;
			var re = new RegExp(regex);

			// vyhovuje hash regexu?
			if (re.test(str)) {
				// ano, url obsahuje hash s referenci:
				chci_vypsat_referenci = true;
			    
			}
			
		}
		if (!chci_vypsat_referenci) {
			// nechci vypat referenci
			var href = jQuery(".info-o-firme #spolecnost_odkaz").attr("href");
			var parser = document.createElement('a');
			parser.href = href;

			// ovsem jen pokud se to tyka redakcni navstevy, ktera obsahuje referenci - pr clanek menit nechceme
			if (parser.hash == "#reference") {
				var new_hash = "#redakcni-navstevy"; // odkaz na blok redakcnich navstev na detailu firmy
				var new_text = "Další návštěvy";

				// odmazat puvodni hash
				var new_href = href.split('#')[0] + new_hash;

				console.log(new_href);

				jQuery(".info-o-firme #spolecnost_odkaz").attr("href", new_href);
				jQuery(".info-o-firme #spolecnost_odkaz").text(new_text); // textik buttonku	
			}

		}
		
	}
    /*
    jQuery( document ).tooltip({
		items: ".tooltip.overena-firma",
		content: function() {
			var element = jQuery( this );
			if ()
			if ( element.is( "[data-geo]" ) )
			{
				var text = element.text();
				return "<img class='map' alt='" + text +
					"' src='http://maps.google.com/maps/api/staticmap?" +
					"zoom=11&size=350x350&maptype=terrain&sensor=false&center=" +
					text + "'>";
			}
			if ( element.is( "[title]" ) )
			{
				return element.attr( "title" );
			}
			if ( element.is( "img" ) )
			{
				return element.attr( "alt" );
			}
		}
	});
	*/
	
	/* xart:hk | 2016-10-18 | Detekce modulu s reklamou */
	if (!(jQuery('.header0').find('.mod_banners-top').length > 0))
	{
		jQuery('.header0').removeClass('has-top-banner');
	}

	jQuery('#ids_filter_wrapper').hide();
	jQuery('a.sort[href="#filtr-firem"]').click(function(){
		jQuery('#ids_filter_wrapper').slideToggle('slow');
		return false;
	});
	

});

/* xart:hk | 2016-01-26 | Recaptcha callback */
function recaptchaCallback() {
	jQuery("input.recaptcha-disabled").prop('disabled', false);
};
